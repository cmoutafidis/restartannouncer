package com.goldencraftnetwork.restartannouncer.models;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class RestartAnnouncerConfig {

    private Boolean enabled;
    private String shutDownMessage;
    private TitleConfigModel title;
    private ArrayList<Integer> warnings;

    public RestartAnnouncerConfig() {
        this.title = new TitleConfigModel();
    }

    public void buildConfig(final FileConfiguration fileConfiguration) {
        this.enabled = fileConfiguration.getBoolean("enabled");
        this.shutDownMessage = fileConfiguration.getString("shutdownmessage");
        this.title.setMain(fileConfiguration.getString("title.main"));
        this.title.setSubtitle(fileConfiguration.getString("title.subtitle"));
        this.warnings = new ArrayList<Integer>(fileConfiguration.getIntegerList("warnings"));
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }

    public String getShutDownMessage() {
        return this.shutDownMessage;
    }

    public void setShutDownMessage(final String shutDownMessage) {
        this.shutDownMessage = shutDownMessage;
    }

    public TitleConfigModel getTitle() {
        return this.title;
    }

    public void setTitle(final TitleConfigModel title) {
        this.title = title;
    }

    public ArrayList<Integer> getWarnings() {
        return this.warnings;
    }

    public void setWarnings(final ArrayList<Integer> warnings) {
        this.warnings = warnings;
    }
}
