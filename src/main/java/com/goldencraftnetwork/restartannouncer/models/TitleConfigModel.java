package com.goldencraftnetwork.restartannouncer.models;

public class TitleConfigModel {

    private String main;
    private String subtitle;

    public String getMain() {
        return this.main;
    }

    void setMain(final String main) {
        this.main = main;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

}
