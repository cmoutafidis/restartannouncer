package com.goldencraftnetwork.restartannouncer;

import com.goldencraftnetwork.restartannouncer.commands.RestartCommands;
import com.goldencraftnetwork.restartannouncer.listeners.RestartListener;
import com.goldencraftnetwork.restartannouncer.models.RestartAnnouncerConfig;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class RestartAnnouncer extends JavaPlugin {

    private static Plugin plugin;
    private static RestartAnnouncerConfig configObject;

    public static Plugin getPlugin() {
        return RestartAnnouncer.plugin;
    }

    public static void reloadConfigObject() {
        RestartAnnouncer.configObject = new RestartAnnouncerConfig();
        RestartAnnouncer.configObject.buildConfig(RestartAnnouncer.getPlugin().getConfig());
    }

    public static RestartAnnouncerConfig getConfigObject() {
        return RestartAnnouncer.configObject;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        RestartAnnouncer.plugin = this;
        this.registerCommands();
        this.registerEvents();
        this.saveDefaultConfig();
        RestartAnnouncer.configObject = new RestartAnnouncerConfig();
        RestartAnnouncer.configObject.buildConfig(this.getConfig());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void registerEvents() {
        this.getServer().getPluginManager().registerEvents(new RestartListener(), this);
    }

    private void registerCommands() {
        this.getCommand("restartAnnouncer").setExecutor(new RestartCommands());
    }

}
