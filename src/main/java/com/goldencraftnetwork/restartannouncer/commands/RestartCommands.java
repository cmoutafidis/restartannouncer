package com.goldencraftnetwork.restartannouncer.commands;

import com.goldencraftnetwork.restartannouncer.RestartAnnouncer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public class RestartCommands implements CommandExecutor {

    private final Plugin plugin = RestartAnnouncer.getPlugin();

    public boolean onCommand(final CommandSender commandSender, final Command command, final String s, final String[] strings) {
        if ("reload".equals(strings[0])) {
            this.plugin.reloadConfig();
            RestartAnnouncer.reloadConfigObject();
            commandSender.sendMessage("Config Reloaded");
            return true;
        }
        return false;
    }
}
