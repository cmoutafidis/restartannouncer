package com.goldencraftnetwork.restartannouncer.tasks;

import com.goldencraftnetwork.restartannouncer.RestartAnnouncer;
import com.goldencraftnetwork.restartannouncer.models.RestartAnnouncerConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class AnnouncerTask extends BukkitRunnable {

    private final Plugin plugin;
    private final int seconds;

    public AnnouncerTask(final Plugin plugin, final int seconds) {
        this.plugin = plugin;
        this.seconds = seconds;
    }

    private static String formatColors(final String string) {
        String line = string;
        line = line.replaceAll("&0", ChatColor.BLACK + "");
        line = line.replaceAll("&1", ChatColor.DARK_BLUE + "");
        line = line.replaceAll("&2", ChatColor.DARK_GREEN + "");
        line = line.replaceAll("&3", ChatColor.DARK_AQUA + "");
        line = line.replaceAll("&4", ChatColor.DARK_RED + "");
        line = line.replaceAll("&5", ChatColor.DARK_PURPLE + "");
        line = line.replaceAll("&6", ChatColor.GOLD + "");
        line = line.replaceAll("&7", ChatColor.GRAY + "");
        line = line.replaceAll("&8", ChatColor.DARK_GRAY + "");
        line = line.replaceAll("&9", ChatColor.BLUE + "");
        line = line.replaceAll("&a", ChatColor.GREEN + "");
        line = line.replaceAll("&b", ChatColor.AQUA + "");
        line = line.replaceAll("&c", ChatColor.RED + "");
        line = line.replaceAll("&d", ChatColor.LIGHT_PURPLE + "");
        line = line.replaceAll("&e", ChatColor.YELLOW + "");
        line = line.replaceAll("&f", ChatColor.WHITE + "");

        line = line.replaceAll("&l", ChatColor.BOLD + "");
        line = line.replaceAll("&m", ChatColor.STRIKETHROUGH + "");
        line = line.replaceAll("&n", ChatColor.UNDERLINE + "");
        line = line.replaceAll("&o", ChatColor.ITALIC + "");
        line = line.replaceAll("&r", ChatColor.RESET + "");
        return line;
    }

    public void run() {
        final RestartAnnouncerConfig config = RestartAnnouncer.getConfigObject();
        for (final Player player : Bukkit.getOnlinePlayers()) {
            player.sendTitle(AnnouncerTask.formatColors(config.getTitle().getMain().replace("%seconds%", Integer.toString(this.seconds))),
                    AnnouncerTask.formatColors(config.getTitle().getSubtitle().replace("%seconds%", Integer.toString(this.seconds))));
        }
    }
}
