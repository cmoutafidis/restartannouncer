package com.goldencraftnetwork.restartannouncer.tasks;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class RestartTask extends BukkitRunnable {
    public void run() {
        Bukkit.spigot().restart();
    }
}
