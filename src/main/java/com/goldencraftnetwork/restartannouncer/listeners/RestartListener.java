package com.goldencraftnetwork.restartannouncer.listeners;

import com.goldencraftnetwork.restartannouncer.RestartAnnouncer;
import com.goldencraftnetwork.restartannouncer.models.RestartAnnouncerConfig;
import com.goldencraftnetwork.restartannouncer.tasks.AnnouncerTask;
import com.goldencraftnetwork.restartannouncer.tasks.RestartTask;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;

public class RestartListener implements Listener {

    private final Plugin plugin = RestartAnnouncer.getPlugin();

    @EventHandler
    public void onRestartCommand(final ServerCommandEvent event) {
        if (event.getCommand().toLowerCase().startsWith("stop")) {
            final RestartAnnouncerConfig config = RestartAnnouncer.getConfigObject();
            if (config.getEnabled()) {
                event.setCancelled(true);
                for (final Integer seconds : config.getWarnings()) {
                    new AnnouncerTask(this.plugin, seconds).runTaskLater(this.plugin, (config.getWarnings().get(0) - seconds) * 20);
                }
                new RestartTask().runTaskLater(this.plugin, config.getWarnings().get(0) * 20);
            }
        }
    }
}
