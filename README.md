A simple plugin for notifying players before an upcomming server restart by sending them a Title.

Config Example:

```
enabled: true

# shutdownmessage is the message you get once the server restarts
shutdownmessage: "&cThe server is now restarting."
title: # change the title and subtitle
  main: "&4&lALERT"
  subtitle: "&cServer is restarting in %seconds% seconds."
  fadein: 10
  stay: 70
  fadeout: 20
warnings: # warnings are displayed every x seconds
  - 120
  - 60
  - 30
  - 15
  - 10
  - 9
  - 8
  - 7
  - 6
  - 5
  - 4
  - 3
  - 2
  - 1
```

After running /restartAnnouncer restart the server will restart in X seconds. 
X is the first number in the warnings list.

You can run /restartAnnouncer reload to reload the config file.